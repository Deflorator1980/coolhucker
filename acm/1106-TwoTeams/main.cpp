#include <iostream>
#include <vector>

using AdjLists = std::vector<std::vector<int>>;

enum Team
{
    NoTeam,
    FirstTeam,
    SecondTeam
};

std::vector<Team> memberTeams;
AdjLists graph;

Team otherTeam(Team team)
{
    return team == FirstTeam ?
                SecondTeam :
                FirstTeam;
}

void dfs(int i)
{
    for(int fId : graph[i])
    {
        if(memberTeams[fId] == NoTeam)
        {
            memberTeams[fId] = otherTeam(memberTeams[i]);
            dfs(fId);
        }
    }
}

void printResult(const std::vector<int> &result)
{
    std::cout << result.size() << std::endl;
    for(int i : result) std::cout << i << " ";
    std::cout << std::endl;
}

void solve()
{
    std::vector<int> result;
    for(std::size_t i = 0; i < memberTeams.size(); ++i)
    {
        if(memberTeams[i] == NoTeam)
        {
            memberTeams[i] = FirstTeam;
            dfs(i);
        }
        if(memberTeams[i] == FirstTeam)
        {
            result.push_back(i+1);
        }
    }

    printResult(result);
}

void run(std::istream &stream)
{
    int nMembers;
    stream >> nMembers;
    memberTeams = std::vector<Team>(nMembers, NoTeam);
    graph.resize(nMembers);
    for(int i = 0; i < nMembers; ++i)
    {
        while (true)
        {
            int friendId;
            stream >> friendId;
            if(friendId == 0) break;
            graph[i].push_back(friendId-1);
        }
    }

    solve();
}

int main()
{
    run(std::cin);
    return 0;
}
