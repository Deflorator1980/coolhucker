#include <iostream>
#include <vector>
#include <algorithm>


struct Team
{
    long id;
    int result;
};

void run(std::istream &stream)
{
    int nTeams;
    stream >> nTeams;

    std::vector<Team> teams;
    teams.reserve(nTeams);

    for(int i = 0; i < nTeams; ++i)
    {
        Team team;
        stream >> team.id >> team.result;
        teams.push_back(team);
    }

    std::stable_sort(teams.begin(), teams.end(), [] (const Team &t1, const Team &t2)
    {
        return t1.result > t2.result;
    });

    for(const Team &team : teams)
    {
        std::cout << team.id << " " << team.result << std::endl;
    }
}

int main()
{
    run(std::cin);
    return 0;
}
