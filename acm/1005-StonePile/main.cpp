#include <iostream>
#include <vector>
#include <bitset>
#include <limits>

constexpr size_t MAX_STONES = 20;
using Combination = std::bitset<MAX_STONES>;

class Problem
{
public:
    explicit Problem(std::istream &stream)
    {
        int numStones;
        stream >> numStones;
        _stones.resize(numStones);

        for(int i = 0; i < numStones; ++i)
        {
            stream >> _stones[i];
        }
    }

    void run()
    {
        int minDiff = std::numeric_limits<int>::max();
        const int maxGen = 1 << _stones.size();
        for(int i = 1; i < maxGen; ++i)
        {
            const int diff = difference(i);
            if(diff < minDiff) minDiff = diff;
            if(minDiff == 0) break;
        }

        std::cout << minDiff << std::endl;
    }

private:
        std::vector<int> _stones;

        int difference(int nCase)
        {
            const Combination combination(nCase);
            int leftSum = 0; int rightSum = 0;
            for(std::size_t i = 0; i < _stones.size(); ++i)
            {
                if(combination[i])
                {
                    leftSum += _stones[i];
                }
                else
                {
                    rightSum += _stones[i];
                }
            }
            return std::abs(leftSum - rightSum);
        }
};

int main()
{
    Problem problem(std::cin);
    problem.run();
    return 0;
}
