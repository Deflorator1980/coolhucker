#include <iostream>
#include <cstdlib>
#include <vector>

struct Member
{
    int value;
    int maximum;
};

std::vector<Member> sequence{{0, 0}, {1, 1}};

Member computeMember(int index)
{
    std::div_t divResult = std::div(index, 2);
    int value = sequence[divResult.quot].value;
    if (divResult.rem != 0) value += sequence[divResult.quot + 1].value;
    int maximum = std::max(sequence[index - 1].maximum, value);

    return Member{value, maximum};
}

void processCase(unsigned n)
{
    if(n >= sequence.size())
    {
        for(std::size_t i = sequence.size(); i < n+1; ++i)
        {
            sequence.push_back(computeMember(i));
        }
    }

    std::cout << sequence[n].maximum << std::endl;
}

int main()
{
    while(true)
    {
        unsigned n;
        std::cin >> n;
        if(n == 0) return 0;
        processCase(n);
    }

    return 0;
}

