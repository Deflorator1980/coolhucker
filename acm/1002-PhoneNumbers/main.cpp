#include <iostream>
#include <sstream>
#include <vector>
#include <map>
#include <algorithm>

using namespace std;

string wordToNum(const string &word)
{
    static map<char, char> cmap
    {
      {'o', '0'}, {'q', '0'}, {'z', '0'},
      {'i', '1'}, {'j', '1'},
      {'a', '2'}, {'b', '2'}, {'c', '2'},
      {'d', '3'}, {'e', '3'}, {'f', '3'},
      {'g', '4'}, {'h', '4'},
      {'k', '5'}, {'l', '5'},
      {'m', '6'}, {'n', '6'},
      {'p', '7'}, {'r', '7'}, {'s', '7'},
      {'t', '8'}, {'u', '8'}, {'v', '8'},
      {'w', '9'}, {'x', '9'}, {'y', '9'},
    };
    stringstream stream;
    for(char c : word)
    {
        stream.put(cmap[c]);
    }

    return stream.str();
}

vector<size_t> findAllPositions(const string &str, const string &substr)
{
    vector<size_t> positions;
    size_t pos = str.find(substr);
    while(pos != string::npos)
    {
        positions.push_back(pos);
        pos = str.find(substr, pos+1);
    }

    return positions;
}

struct Test
{
    string number;
    vector<string> words;
};

class PhoneNumber
{
public:
    explicit PhoneNumber(const Test &test) : _test(test)
    {
        map<string, size_t, greater<string>> convertedWords;
        for(size_t i = 0; i < _test.words.size(); ++i)
        {
            convertedWords.insert({wordToNum(_test.words[i]), i});
        }

        for(auto &&word : convertedWords)
        {
            const vector<size_t> positions = findAllPositions(_test.number, word.first);
            if(positions.empty()) continue;
            for(size_t pos : positions)
            {
                _wordPositions.insert({pos, word.second});
            }
        }
    }

    void processTest()
    {
        processIndex(0);
        printResult();
    }

private:
    void processIndex(size_t index)
    {
        auto range = _wordPositions.equal_range(index);
        for(auto i = range.first; i != range.second; ++i)
        {
            if(_result.size() > 0 && _result.size() <= _wordChain.size()) return;
            const size_t nextIndex = index + _test.words[(*i).second].size();
            const size_t numberSize = _test.number.size();
            if(nextIndex > numberSize)
            {
                return;
            }

            _wordChain.push_back((*i).second);
            if(nextIndex == numberSize)
            {
                if(_result.empty() || _wordChain.size() < _result.size())
                {
                    _result = _wordChain;
                }
            }
            else
            {
                processIndex(nextIndex);
            }
            _wordChain.pop_back();
        }
    }

    void printResult()
    {
        if(_result.empty())
        {
            cout << "No solution." << endl;
        }
        else
        {
            for(int i : _result)
                cout << _test.words[i] << " ";
            cout << endl;
        }
    }

    const Test &_test;
    multimap<size_t, size_t> _wordPositions;
    vector<int> _result;
    vector<int> _wordChain;
};

void run(istream &stream)
{
    while(true)
    {
        Test test;
        stream >> test.number;
        if(test.number == "-1") return;

        int nwords;
        stream >> nwords;
        test.words.resize(nwords);

        for(string &word : test.words)
        {
            stream >> word;
        }
        PhoneNumber number(test);
        number.processTest();
    }
}

int main()
{
    run(cin);
    return 0;
}
