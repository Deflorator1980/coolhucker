#include <iostream>
#include <vector>
#include <cmath>
#include <iterator>
#include <algorithm>

using namespace std;

int main()
{
    using InIter =  istream_iterator<uint64_t>;
    vector<uint64_t> numbers;
    std::copy(InIter(cin), InIter(), back_inserter(numbers));

    cout.precision(4);
    cout.setf(ios::left);
    cout.setf(ios::fixed);
    std::for_each(numbers.rbegin(), numbers.rend(), [](int64_t n)
    {
        cout << sqrt(n) << endl;
    });

    return 0;
}
