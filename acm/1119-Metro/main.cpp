#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>

using Diag = std::pair<int, int>;

auto diagSortComparer = [](const Diag &d1, const Diag &d2)
    { return d1.first < d2.first || d1.second < d2.second;};

auto diagUniqueComparer = [](const Diag &d1, const Diag &d2)
    { return d1.first == d2.first || d1.second == d2.second;};

void run(std::istream &stream)
{
    int n, m;
    stream >> n >> m;

    int count;
    stream >> count;

    std::vector<Diag> diags(count);
    for(int i = 0; i < count; ++i)
    {
        stream >> diags[i].first >> diags[i].second;
    }

    std::sort(diags.begin(), diags.end(), diagSortComparer);
    auto newEnd = std::unique(diags.begin(), diags.end(), diagUniqueComparer);
    std::size_t diagCount = newEnd - diags.begin();

    const int distance = n+m - diagCount * (200 - 100*std::sqrt(2));
    std::cout << distance << std::endl;
}

int main()
{
    std::ifstream fs("input");
    run(fs);
    return 0;
}

